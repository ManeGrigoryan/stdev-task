<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Resume;

class UsersController extends Controller
{
    public function display(Request $request)
    {
        $firstname = (!empty($request->input('firstname'))) ? '%' . $request->input('firstname') . '%' : '%%';
        $lastname = (!empty($request->input('lastname'))) ? '%' . $request->input('lastname') . '%' : '%%';
        $keywords = (!empty($request->input('keywords'))) ? '%' . $request->input('keywords') . '%' : '%%';
        $users = User::where('firstname', 'LIKE', $firstname)->
        Where('lastname', 'LIKE', $lastname)->
        Where('keywords', 'LIKE', $keywords)->
        orderBy('created_at', 'DESC')->paginate(5);
        $user_resumes = User::select('id')->get();
        $resumes = array();
        $resume = array();
        foreach ($user_resumes as $user_resume) {
            $resume['id'] = $user_resume->resume->id;
            $resume['userId'] = $user_resume->resume->userId;
            $resume['name'] = $user_resume->resume->name;
            $resumes[] = $resume;
        }
        return view('users.display')->
        with('users', $users)->
        with('resumes', $resumes)->
        with('firstname', $request->input('firstname'))->
        with('lastname', $request->input('lastname'))->
        with('keywords', $request->input('keywords'));
    }

    public function store(Request $request)
    {
        $users = new User;
        $resumes = new Resume;
        $this->validate($request, [
                'firstName' => 'required',
                'lastName' => 'required',
                'keywords' => 'required',
                'resume' => 'required']
        );
        if ($request->hasFile('resume')) {
            //Get FileName
            $fileNameFull = $request->file('resume')->getClientOriginalName();
            //Get File Extension
            $fileExtension = $request->file('resume')->getClientOriginalExtension();
            //Declare allowed resume file formats
            $allowed_file_types = array('pdf', 'doc', 'docx', 'txt');

            //Validate the format of resume
            if (in_array($fileExtension, $allowed_file_types)) {
                $fileName = pathinfo($fileNameFull, PATHINFO_FILENAME);
                $fileNameToStore = $fileName . '_' . time() . '.' . $fileExtension;
                $path = $request->file('resume')->storeAs('public/uploads', $fileNameToStore);
                $users->firstName = $request->input('firstName');
                $users->lastName = $request->input('lastName');
                $users->keywords = $request->input('keywords');
                $users->save();
                $user_id = $users->id;
                $resumes->source = $path;
                $resumes->name = $fileNameToStore;
                $resumes->type = $fileExtension;
                $resumes->userId = $user_id;
                $resumes->save();
                return redirect('/')->with('success', 'User has been added');
            } else {
                //Still need to understand why am I getting error messages if I validate file format using Validator
                return response('/create')->with('error', 'File format needs to be .pdf, .doc,.docx,.txt');
            }
        }
    }

    public function add()
    {
        return view('users.add');
    }
}
