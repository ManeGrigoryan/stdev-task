<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'source' => $faker->file(),
        'name'=>$faker->name,
        'type'=>$faker->fileExtension,
        'userId'=>random_int(0,999999),
        'remember_token' => str_random(10),

    ];
});
