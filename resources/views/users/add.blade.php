@extends('layouts.users')
@section('content')
    <div class="container">

        <h1> Add New User</h1>
        @include('include.messages')

        <div class="panel panel-default">
            <div class="panel-body">

                <section id="add_user">
                    {!! Form::open(['action' => 'UsersController@store', 'method'=>'POST', 'enctype'=>'multipart/form-data']) !!}
                    <div class="form-group">
                        {{Form::text('firstName','',['class'=>'form-control','placeholder'=>'First Name'])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('lastName','',['class'=>'form-control','placeholder'=>'Last Name'])}}
                    </div>
                    <div class="form-group">
                        {{Form::textarea('keywords','',['class'=>'form-control','placeholder'=>'Key Words Describing The Person '])}}
                    </div>
                    <div class="form-group">
                        {{Form::file('resume')}}
                    </div>
                    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
                    {!! Form::close() !!}
                </section>
            </div>
        </div>
    </div>
@endsection
