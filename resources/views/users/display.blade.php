@extends('layouts.users')
@section('content')

    <div class="container">
        @include('include.messages')

        <h1> Users</h1>
        <hr>
        <div class="form-group">
            <button class="btn btn-info" onclick="sectionFunction('search','search_arrow')">
                <i id="search_arrow" class="glyphicon glyphicon-arrow-down">
                </i>
                Search Users
            </button>
        </div>
        {{--<hr>--}}

        <section id="search" style="display:none;">
            <form method="get" id="search_form">
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" class="form-control" name="firstname" placeholder="First Name"
                                   value="{{$firstname}}">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" class="form-control" name="lastname" placeholder="Last Name"
                                   value="{{$lastname}}">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" class="form-control" name="keywords" placeholder="Keywords"
                                   value="{{$keywords}}">
                        </div>
                    </div>
                </div>
                <div class="form-group input-group">
                    <button onclick="document.getElementById('search_form').submit()"
                            class="btn btn-primary">Search
                    </button>
                </div>
            </form>
            <hr>
        </section>
        <div class="panel panel-default">
            <div class="panel-body">
                @if(count($users)>0)
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Keywords</th>
                            <th>Resumes</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)


                            <tr>
                                <td>{{$user->firstName}}</td>
                                <td>{{$user->lastName}}</td>
                                <td>{{$user->keywords}}</td>
                                @foreach($resumes as $resume)
                                    @if($resume['userId'] == $user->id)
                                        <td><a href="/storage/uploads/{{$resume['name']}}"
                                               download="{{$resume['name']}}">
                                                <button class="btn btn-primary">
                                                    <i class="glyphicon glyphicon-download">
                                                    </i>
                                                    Download
                                                </button>
                                            </a></td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
        {{$users->links()}}
        @else
            <h3>No user was found!</h3>
    </div>

    @endif
@endsection