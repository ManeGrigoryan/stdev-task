<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mane Grigoryan-STDev Task</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script   type="text/javascript" src="{{asset('/js/myFunctions.js')}}"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">STDev Task</a>
        </div>
        <ul class="nav navbar-nav" style="display: inline">

            <li ><a href="/">Users</a></li>
            <li><a href="/create">Add User</a></li>
        </ul>
    </div>
</nav>
@yield('content')
</body>
</html>

