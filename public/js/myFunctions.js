function sectionFunction(section, arrow) {
    var obj = document.getElementById(section);
    console.log(obj.style.display);
    if (obj.style.display == "none") {
        obj.style.display = "block";

    } else if (obj.style.display == "block") {
        obj.style.display = "none";
    }
    changeArrow(arrow);
}

function changeArrow(id) {
    var arrow;
    arrow = document.getElementById(id);
    if (arrow.className == 'glyphicon glyphicon-arrow-down') {
        arrow.className = 'glyphicon glyphicon-arrow-up'
    } else {
        arrow.className = 'glyphicon glyphicon-arrow-down';
    }
}

